#!/usr/bin/env bash

apt-get update && apt-get -y upgrade
apt-get -y install software-properties-common
add-apt-repository ppa:ondrej/php
apt-get update

echo "Installing php7.3 and modules.."
apt-get -y install php7.3 unzip
apt-get -y install php-pear php7.3-curl php7.3-dev php7.3-gd php7.3-mbstring php7.3-zip php7.3-mysql php7.3-xml

echo "Installing composer.."
/vagrant/scripts/composer.sh

echo "Installing Node.."
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install --lts

echo "Done provisioning.."

# Set PATH for composer
# Add this to .profile
# if [ -d "$HOME/.composer/vendor/bin" ] ; then
#     PATH="$HOME/.composer/vendor/bin:$PATH"
# fi
